package com.bibao.dao;

import java.util.List;

import com.bibao.model.Person;

public interface PersonDao {
	public Person findById(int id);
	public List<Person> findAll();
	public Person save(Person person);
	public Person update(Person person);
	public void deleteById(int id);
}
