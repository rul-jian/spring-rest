package com.bibao.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.bibao.dao.PersonDao;
import com.bibao.model.Person;

@RestController
@RequestMapping("/person")
public class PersonRestService {
	@Autowired
	private PersonDao personDao;
	
	@GetMapping("/ping")
	public String ping() {
		return "ping service successfully";
	}
	
	@GetMapping("/{id}")
	public Person getPersonById(@PathVariable("id") int id) {
		return personDao.findById(id);
	}
	
	@GetMapping
	public List<Person> getAllPerson() {
		return personDao.findAll();
	}
	
	@PostMapping
	public Person savePerson(@RequestBody Person person) {
		Person savedPerson = personDao.save(person);
		return savedPerson;
	}
	
	@PutMapping
	public Person updatePerson(@RequestBody Person person) {
		Person updatedPerson = personDao.update(person);
		return updatedPerson;
	}
	
	@DeleteMapping("/{id}")
	public void deletePersonById(@PathVariable("id") int id) {
		personDao.deleteById(id);
	}
}
